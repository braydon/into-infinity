"""
Into-Infinity Exibition
Author: Braydon Fuller (braydon@dublab.com)
Main Dependencies: ImageMagick, Ming, FFMpeg, CherryPy
License: GNU GPL <http://www.gnu.org/licenses/gpl.txt>
"""

import os
from copy import copy
from datetime import datetime
from functools import partial
from os.path import getmtime, exists
import random
from urllib import pathname2url as urlize
from urllib import url2pathname as unurlize
import tarfile
import uuid

from ming import *
import cherrypy
from cherrypy._cperror import HTTPError
import simplejson
from Cheetah.Template import Template


class NoSuchFile(Exception):
    pass

class SharedState:
    __shared_state = {}
    def __init__(self):
        self.__dict__ = self.__shared_state

st = SharedState()
st.making_archive = False        

class Document(object):
    """A basic document object class."""
    def __init__(self, **kwargs):
        self.doc_path = '/var/local/intoinfinity/documents/'
        self.__name__ = "Document"
        self._use(kwargs)

    def _use(self, kwargs):
        if isinstance(kwargs, dict):
            for key, value in kwargs.items():
                self.__setattr__(key, value)
        else:
            raise 

    def _strip_ext(self, file_path):
        f = file_path.split(".")
        return ".".join(f[0:len(f)-1])

    def _replace_ext(self, file_path, new_ext):
        f = file_path.split(".")
        return ".".join(f[0:len(f)-1]) + new_ext

    def to_json(self):
        doc_dict = self.__dict__
        for key, value in doc_dict.items():
            if key[0] == "_" and key[-1] == "_":
                del doc_dict[key]
        return simplejson.dumps(doc_dict)

class Ear(Document):
    """Document object for audio loops."""
    def __init__(self, **kwargs):
        super(Ear,self).__init__(**kwargs)
        self.__name__ = "Ear"
        if hasattr(self, "file_path"):
            self.swf_file_path = self.make_swf_with_wav()

    def make_swf_with_wav(self):
        """
        Creates a looping SWF file of the WAV file.
        """
        swf_file_path = self._replace_ext(self.file_path,".swf")
        org = self.doc_path + self.file_path
        swf_abspath = self.doc_path + swf_file_path
        if not exists(swf_abspath) or getmtime(org) > getmtime(swf_abspath):
            Ming_useSWFVersion(7)
            Ming_setScale(20)
            swf = SWFMovie()
            swf.setRate(12)
            swf.setDimension(1,1)
            swf.setNumberOfFrames(1)
            mp3_file_path = self._replace_ext(self.file_path, ".0.wav")
            org = self.doc_path + self.file_path
            mp3_abspath = self.doc_path + mp3_file_path
            if not exists(mp3_abspath) or getmtime(org) > getmtime(mp3_abspath):
                os.popen("ffmpeg -i %s -ac 1 -ar 22000 %s" % (org, mp3_abspath))
            
            soundobject=SWFSound(self.doc_path + mp3_file_path, SWF_SOUND_NOT_COMPRESSED_LE|SWF_SOUND_22KHZ|SWF_SOUND_16BITS|SWF_SOUND_MONO)
            os.remove(mp3_abspath)
            
            soundinstance = swf.startSound(soundobject)
            soundinstance.setLoopCount(100)
            soundinstance.setLoopInPoint(1 *100)
            swf.save(str(self.doc_path + swf_file_path))
        return swf_file_path

    def __repr__(self):
        return "Document(%s, %s)" % (self.file_path, self.__name__)


class Eye(Document):
    """Document object for visual circle artwork."""
    def __init__(self, **kwargs):
        super(Eye, self).__init__(**kwargs)
        self.__name__ = "Eye"
        if hasattr(self,"file_path"):
            self.gallery_path = self.resize("gallery", "x600")
            self.medium_path = self.resize("medium", "x370")
            self.small_path = self.resize("small", "x150")

    def resize(self, name, pixels):
        """
        Saves a new image converted from the original hi-resolution
        image into a smaller image.
        """
        path = self._strip_ext(self.file_path) + "_"+name+".png"
        org = self.doc_path + self.file_path
        gal = self.doc_path + path
        if not exists(gal) or getmtime(org) > getmtime(gal):
            os.popen('convert -resize %s -colors 500 -depth 16 -quality 95 %s %s' % (pixels, org, gal))
            os.popen('optipng -o5 %s' % (gal))
        return path


class NoSuchAuthor(Exception):
    pass

class NoSuchDocumentType(Exception):
    pass

def preduce(fn,ls,a):
    ls.insert(0,a)
    return reduce(fn,ls)

def has_document(a, document_type):
    if a[1].has_key(document_type) and len(a[1][document_type]) > 0:
        return True
    else:
        return False

def no_keywords(path, keywords):
    path = path.lower()
    if any(path.find(value) >= 0 for value in keywords):
        return False
    else:
        return True

def invert(a,b,key):
    """
    This is to be used with a 'partial' and a '(p)reduce' function.
    Partial to create a new function with the key.
    Preduce to process b into an accumlated a.
    This takes the key from b and adds the value(s)
    as (a) key(s) into an accumulated a.
    
    Example Usage:
    result = preduce(partial(invert,key=key), index.items(), {})
        
    """
    value = b[1][key]
    if isinstance(value, list):
        for item in value:
            a[item] = b[0]
    elif isinstance(value, str):
        a[value] = b[0]
    else:
        raise Exception
    return a


def extract_filenames(a,b,key,extension):
    """
    This is to be used with a 'partial' and a '(p)reduce' function.
    Partial to create a new function with the key.
    Preduce to process b into an accumlated a.
    This takes the key from b and adds the value(s)
    as (a) key(s) into an accumulated a.
    
    Example Usage:
    result = preduce(partial(extract_filenames,key=key), index.items(), {})
        
    """
    value = b[1][key]
    if isinstance(value, list):
        for item in value:
            if not extension or extension.lower() == "false":
                a.append(item.split(".")[0])
            elif extension or extension.lower() == "true":
                a.append(item)
    elif isinstance(value, str):
        a.append(item.split(".")[0])
    else:
        raise Exception
    return a


class DocumentDB(object):
    """
    This is a collection of functions to be used to create
    view-indexes, as-well as query results from the database.
    """

    def __init__(self, document_types, index):
        """
        Examples:
        document_types = {"Eye":Eye(),"Ear":Ear()}
        index = The name of the file to store the index.
        """
        self.document_types = document_types
        self.doc_path = '/var/local/intoinfinity/documents/'
        self.index = self.doc_path + index

    def open_view(self, key=False):
        """
        Opens an index or view index, and prepares it for further use.
        """
        if key:
            return simplejson.load(open(self.index+"."+key))
        else:
            return simplejson.load(open(self.index))

    def save_view(self, key):
        """
        To be used to build a sub index based on a key, such as Eye and Ear, or Location.
        """
        index = simplejson.load(open(self.index))
        result = simplejson.dumps(preduce(partial(invert,key=key), index.items(), {}))
        f = open(self.index+"."+key,'w')
        f.write(result)
        f.close()

    def save_view_by_author(self, author):
        """
        Saves a view index that includes a dictionary of all documents
        by the person along with the document type.
        """
        view = self.open_view()
        try:
            item = view[author]
        except:
            raise NoSuchAuthor
        result = {}
        for key, value in self.document_types.items():
            for document in item[key]:
                result[document] = key

        f = open(self.index+"."+author,'w')
        f.write(simplejson.dumps(result))
        f.close()

    def get_authors(self, document_type):
        """
        Filters the main index into a dictionary of only the authors that have
        documents of the matched type.
        """
        if any(document_type == key for key, value in self.document_types.items()):
            index = self.open_view()
            return filter(partial(has_document,document_type=document_type), index.items())
        else:
            raise NoSuchDocumentType


    def _into_doc(self, author, file, meta, document_type):
        kwargs = {"author":author, "location":meta["Location"], "file_path":file, "url":meta["Url"]}
        doc = self.document_types[document_type]
        doc.__init__(**kwargs)
        return copy(doc)

    def get_authors_with_documents(self, document_type, as_objects=True):
        """
        Filters the main index into a dictionary of only the authors that have
        documents of the matched type. AND. returns an object for the document_type.
        """
        index = self.open_view()
        self.r = filter(partial(has_document,document_type=document_type), index.items())
        if as_objects:
            ret = {}
            for i in self.r:
                for file in i[1][document_type]:
                    if ret.has_key(i[0]) and isinstance(ret[i[0]], list):
                        ret[i[0]].append(self._into_doc(i[0], file, i[1], document_type))
                    else:
                        ret[i[0]] = [self._into_doc(i[0], file, i[1], document_type)]
            return ret
        else:
            return self.r

    def get_total_possible(self):
        """
        Determines the total number of possible combinations
        of each combination of document type.
        """
        keys = self.document_types.keys()
        total = 1
        for key in keys:
            total = total * len(self.open_view(key))
        return total

    def get_random_by_author(self, author):
        """
        Returns any document by an Author.
        """
        self.save_view_by_author(author)
        mix = self.open_view(author)
        file_path, document_type = mix.items()[random.randrange(len(mix))]
        item = self.open_view()[author]
        kwargs = {"author":author, "location":item["Location"],"file_path":file_path, "url":item["Url"]}
        doc = self.document_types[document_type]
        doc.__init__(**kwargs)
        return doc
        
    def get_random(self, document_type):
        """
        To be used to get either a random "Eye" or "Ear" object 
        from the database.
        """
        self.save_view(document_type)
        view = self.open_view(document_type)
        index = self.open_view()
        file_path, author = view.items()[random.randrange(len(view))]
        item = index[author]
        kwargs = {"author":author, "location":item["Location"], "file_path":file_path, "url":item["Url"]}
        doc = self.document_types[document_type]
        doc.__init__(**kwargs)
        return doc

    def get_documents_package(self, document_type, compression="bz2"):
        """
        Return the name of the tar file, and create the compressed
        archive if it needs to be updated.
        """
        tar_filename = "intoinfinity_"+document_type+".tar.%s" % compression
        tar_path = self.doc_path+tar_filename
        if not exists(tar_path) or getmtime(self.index) > getmtime(tar_path):
            self.save_view(document_type)
            view = self.open_view(document_type)
            files = view.keys()
            print files
            tar = tarfile.open(tar_path,'w:%s' % compression)
            for name in files:
                try:
                    tar.add(self.doc_path+name)
                except:
                    print "Could not find %s to compress" % name                    
            tar.close()
        size = str(round(os.path.getsize(tar_path) / 1024.0 / 1024.0, 2))+"MB"
        print "Done compressing %s" % document_type
        return tar_filename, size

    def get_site_package(self, filename, compression="bz2", filter_out_keywords=[".tar.gz",".tar.bz2",".log",".swf"]):
        """
        Creates a tar of the entire site depending if the index file is newer than
        the existing archive.
        """
        index = self.open_view() #just to make sure that the index is good.
        tar_filename = filename+".tar.%s" % compression
        tar_path = self.doc_path+tar_filename
        if not exists(tar_path) or getmtime(self.index) > getmtime(tar_path):
            tar = tarfile.open(tar_path,'w:%s' % compression)
            files = []
            for t in os.walk('/var/local/intoinfinity'):
                for f in t[2]:
                    files.append("/".join((t[0], f)))
            files = filter(partial(no_keywords, keywords=filter_out_keywords), files)
            print files
            for name in files:
                try:
                    tar.add(name)
                except:
                    print "Could not find %s to compress" % name
            tar.close()
        size = str(round(os.path.getsize(tar_path) / 1024.0 / 1024.0, 2))+"MB"
        print "Done compressing %s" % filename
        return tar_filename, size

def alpha_compare(x, y):
    x = x[0].lower()
    y = y[0].lower()
    if x > y:
        return 1
    elif x == y:
        return 0
    elif x < y:
        return -1

EAR_EYE_DB = DocumentDB({"Eye":Eye(),"Ear":Ear()},'index')


class JSONCommands(object):
    def __init__(self):
        self.db = EAR_EYE_DB

    @cherrypy.expose
    def filenames(self, document_type, extension=False):
        filtered = self.db.get_authors_with_documents(document_type, as_objects=False)
        return simplejson.dumps(preduce(partial(extract_filenames,key=document_type,extension=extension), filtered, []))

    @cherrypy.expose
    def random(self, document_type):
        doc = self.db.get_random(document_type)
        return doc.to_json()

class Mixer(object):
    """
    This exposes all url for the Sound Mixer.
    """

    def __init__(self):
        self.db = EAR_EYE_DB

    def sort_dict(self, x):
        result = []
        for x, y in x.items():
            result.append((x,y))
        result.sort(alpha_compare)
        return result

    @cherrypy.expose
    def default(self):
        ear_authors = self.db.get_authors_with_documents("Ear")
        ear_authors = self.sort_dict(ear_authors)
        t = Template(file="/var/local/intoinfinity/mixer.tmpl", searchList=[locals()])
        return t.respond()

class Nesting(object):
    """
    Definitions for the nesting visual exhibition.
    """

    def __init__(self):
        self.db = EAR_EYE_DB

    @cherrypy.expose
    def default(self):
        t = Template(file="/var/local/intoinfinity/nesting.tmpl", searchList=[locals()])
        return t.respond()


class Exhibition(object):
    """
    This exposes all the URL for the exhibition.
    """

    def __init__(self):
        self.db = EAR_EYE_DB

    @cherrypy.expose
    def about(self):
        t = Template(file="/var/local/intoinfinity/about.tmpl", searchList=[locals()])
        return t.respond()

    @cherrypy.expose
    def downloads(self):
        if not st.making_archive:
            st.making_archive = True
            date = datetime.fromtimestamp(getmtime(self.db.index))
            eye_tar, eye_size = self.db.get_documents_package("Eye")
            ear_tar, ear_size = self.db.get_documents_package("Ear")
            everything, everything_size = self.db.get_site_package(filename="into_infinity_web_exhibition")
            code, code_size = self.db.get_site_package(filename="into_infinity_web_exhibition_code", \
                                       filter_out_keywords=[".jpg",".jpeg",".swf",".tif",".tiff",".png",".aiff",".aif",".wav", ".tar.gz",".tar.bz2",".pyc",".zip",".log"])
            t = Template(file="/var/local/intoinfinity/downloads.tmpl", searchList=[locals()])
            st.making_archive = False
            return t.respond()
        else:
            return "Currently processing downloads, please check back soon. <a href='/exhibition'>Return to Exhibition</a>"

    @cherrypy.expose
    def artists(self):
        ears = self.db.get_authors("Ear")
        eyes = self.db.get_authors("Eye")
        eyes.sort(alpha_compare)
        ears.sort(alpha_compare)
        t = Template(file="/var/local/intoinfinity/artists.tmpl", searchList=[locals()])
        return t.respond()

    @cherrypy.expose
    def default(self, *args):
        if args:
            try:
                a = self.db.get_random_by_author(unurlize(args[0]))
            except NoSuchAuthor:
                m = "The path '/exhibition/%s' was not found." % (unurlize(args[0]))
                raise HTTPError(404, message=m)
            if isinstance(a, self.db.document_types["Ear"].__class__):
                eye = self.db.get_random("Eye")
                ear = a
            elif isinstance(a, self.db.document_types["Eye"].__class__):
                eye = a
                ear = self.db.get_random("Ear")
            artist = unurlize(args[0])
        else:
            eye = self.db.get_random("Eye")
            ear = self.db.get_random("Ear")

        t = Template(file="/var/local/intoinfinity/exhibition.tmpl", searchList=[locals()])
        return t.respond()            

class Root(object):
    """
    This is the class that exposes the web application.
    It is the root URL and all URLs are mounted here.
    """

    def __init__(self):
        self.db = EAR_EYE_DB
        self.mixer = Mixer()
        self.nesting = Nesting()
        self.exhibition = Exhibition()        
        self.json = JSONCommands()
        self.launch_date = datetime(2008, 8, 8, 0, 0, 00, 000000)

    @cherrypy.expose
    def index(self):
        return open('/var/local/intoinfinity/index.html','r').read()

if __name__ == "__main__":
    cherrypy.quickstart(Root(), '/', config="/var/local/intoinfinity/settings.conf")
